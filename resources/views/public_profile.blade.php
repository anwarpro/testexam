@extends('layouts.app')

@push('metas')

    <meta property="og:title" content="{{ $user->name }}">
    <meta property="og:description" content="Offering tour packages for individuals or groups.">
    <meta property="og:image" content="http://euro-travel-example.com/thumbnail.jpg">
    <meta property="og:url" content="http://euro-travel-example.com/index.htm">

    <meta name="twitter:title" content="European Travel Destinations ">
    <meta name="twitter:description" content=" Offering tour packages for individuals or groups.">
    <meta name="twitter:image" content=" http://euro-travel-example.com/thumbnail.jpg">
    <meta name="twitter:card" content="summary_large_image">

@endpush

@section('content')
    <div>
        Shareable link {{ $url }}
        Welcome {{$user->name}}
    </div>
@endsection
